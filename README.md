# Offloader Setup for parish halls

## Features avaliable
- ssh-key-dropping
- Batman_V
- fastd
- vlans 123 and 124 for ffbsee and ffbsee_mesh
- vlans 666 and 667 for ffmuc and ffmuc_mesh

## Features not yet ready
- repeat ifup for second bridge (the first one works, the second one has to receive an extra invitation by "ifup")

## Take care for vlan-config on your switch:
- APU has to be on port with all vlans (1 for accesspoints, 1 for clients of each freifunk-community, 1 for mesh of each freifunk-community)
- provide corresponding freifunk-community-vlans (mesh and client) and connection to APU on ports you connect an accesspoint to.

### For example:
- vlan1: for Managment on Port of APU, all AP-Ports and Management-Port
- vlan123: for FFBSEE-Clients on all AP-Ports and APU-Port
- vlan124: for FFBSEE-Mesh on all AP-Ports and APU-Port
- vlan666: for FFMUC-Clients on all AP-Ports and APU-Port
- vlan667: for FFMUC-MESH on all AP-Ports and APU-Port
- for febugging Resons vlan1 could be provided on other ports, tools
- for providing ff on switchports enable vlan123 / vlan666 on those ones.
